package dto;

import java.util.ArrayList;
import java.lang.Math;

public class Mapa{
	
	private int rows;
	private int cols;
	ArrayList<Casilla> tablero;
	
	
	
	public Mapa() {
		this.rows=10;
		this.cols=10;
		this.tablero= this.generaDefault();
	}
	
	/**
	 * @param sizeX: tama�o horizontal
	 * @param sizeY: tama�o vertical
	 */
	public Mapa(int sizeX, int sizeY) {
		super();
		this.rows = sizeX;
		this.cols = sizeY;
		this.tablero= this.generaDefault();
	}

	/**
	 * @param sizeX: tama�o horizontal
	 * @param sizeY: tama�o vertical
	 * @param tablero: hashmap con coordenadas y su valor
	 */
	public Mapa(int sizeX, int sizeY, ArrayList<Casilla> tablero) {
		super();
		this.rows = sizeX;
		this.cols = sizeY;
		this.tablero = tablero;
	}

	/**
	 * @return the sizeX
	 */
	public int getRows() {
		return rows;
	}

	/**
	 * @param sizeX the sizeX to set
	 */
	public void setRows(int sizeX) {
		this.rows = sizeX;
	}

	/**
	 * @return the sizeY
	 */
	public int getCols() {
		return cols;
	}

	/**
	 * @param sizeY the sizeY to set
	 */
	public void setCols(int sizeY) {
		this.cols = sizeY;
	}

	/**
	 * @return the tablero
	 */
	public ArrayList<Casilla> getTablero() {
		return tablero;
	}
	
	

	/**
	 * @param tablero the tablero to set
	 */
	public void setTablero(ArrayList<Casilla> tablero) {
		this.tablero = tablero;
	}

	/**
	 * Devuelve un string con la informacion del tablero
	 * @param t: tablero
	 * @param dimY: numero de columnas
	 * @return tablero para imprimir
	 */
	public String muestraTablero(ArrayList<Casilla> t,int dimY) {
		String aux="";
		Casilla c;
		for (int i = 0; i < t.size(); i++) {
			if(i!=0 && i%dimY==0) {
				aux+="\n";
			}
			c= t.get(i);
			aux+=c.getCodigo()+"\t";
		}
		return aux;
	}
	
	/**
	 * Genera el tablero del enunciado
	 * @return el tablero ya inicializado
	 */
	public ArrayList<Casilla> generaDefault(){
		ArrayList<Casilla> aux = new ArrayList<Casilla>();
		
		aux.add(new Casilla(0,0,1));
		aux.add(new Casilla(0,1,0));
		aux.add(new Casilla(0,2,-1));
		aux.add(new Casilla(0,3,1));
		aux.add(new Casilla(0,4,3));
		aux.add(new Casilla(0,5,2));
		aux.add(new Casilla(0,6,3));
		aux.add(new Casilla(0,7,4));
		aux.add(new Casilla(0,8,3));
		aux.add(new Casilla(0,9,1));
		
		aux.add(new Casilla(1,0,2));
		aux.add(new Casilla(1,1,1));
		aux.add(new Casilla(1,2,-1));
		aux.add(new Casilla(1,3,2));
		aux.add(new Casilla(1,4,4));
		aux.add(new Casilla(1,5,2));
		aux.add(new Casilla(1,6,2));
		aux.add(new Casilla(1,7,4));
		aux.add(new Casilla(1,8,2));
		aux.add(new Casilla(1,9,2));
		
		aux.add(new Casilla(2,0,5));
		aux.add(new Casilla(2,1,3));
		aux.add(new Casilla(2,2,-1));
		aux.add(new Casilla(2,3,2));
		aux.add(new Casilla(2,4,3));
		aux.add(new Casilla(2,5,2));
		aux.add(new Casilla(2,6,-1));
		aux.add(new Casilla(2,7,3));
		aux.add(new Casilla(2,8,3));
		aux.add(new Casilla(2,9,3));
		
		aux.add(new Casilla(3,0,3));
		aux.add(new Casilla(3,1,3));
		aux.add(new Casilla(3,2,1));
		aux.add(new Casilla(3,3,3));
		aux.add(new Casilla(3,4,4));
		aux.add(new Casilla(3,5,3));
		aux.add(new Casilla(3,6,-1));
		aux.add(new Casilla(3,7,1));
		aux.add(new Casilla(3,8,2));
		aux.add(new Casilla(3,9,2));
		
		aux.add(new Casilla(4,0,2));
		aux.add(new Casilla(4,1,2));
		aux.add(new Casilla(4,2,2));
		aux.add(new Casilla(4,3,3));
		aux.add(new Casilla(4,4,6));
		aux.add(new Casilla(4,5,4));
		aux.add(new Casilla(4,6,-1));
		aux.add(new Casilla(4,7,1));
		aux.add(new Casilla(4,8,2));
		aux.add(new Casilla(4,9,1));
		
		aux.add(new Casilla(5,0,-1));
		aux.add(new Casilla(5,1,-1));
		aux.add(new Casilla(5,2,-1));
		aux.add(new Casilla(5,3,-1));
		aux.add(new Casilla(5,4,3));
		aux.add(new Casilla(5,5,3));
		aux.add(new Casilla(5,6,-1));
		aux.add(new Casilla(5,7,0));
		aux.add(new Casilla(5,8,2));
		aux.add(new Casilla(5,9,-1));
		
		aux.add(new Casilla(6,0,-1));
		aux.add(new Casilla(6,1,-1));
		aux.add(new Casilla(6,2,-1));
		aux.add(new Casilla(6,3,-1));
		aux.add(new Casilla(6,4,2));
		aux.add(new Casilla(6,5,4));
		aux.add(new Casilla(6,6,-1));
		aux.add(new Casilla(6,7,2));
		aux.add(new Casilla(6,8,2));
		aux.add(new Casilla(6,9,-1));
		
		aux.add(new Casilla(7,0,2));
		aux.add(new Casilla(7,1,3));
		aux.add(new Casilla(7,2,4));
		aux.add(new Casilla(7,3,3));
		aux.add(new Casilla(7,4,1));
		aux.add(new Casilla(7,5,3));
		aux.add(new Casilla(7,6,-1));
		aux.add(new Casilla(7,7,3));
		aux.add(new Casilla(7,8,2));
		aux.add(new Casilla(7,9,-1));
		
		aux.add(new Casilla(8,0,3));
		aux.add(new Casilla(8,1,5));
		aux.add(new Casilla(8,2,6));
		aux.add(new Casilla(8,3,5));
		aux.add(new Casilla(8,4,2));
		aux.add(new Casilla(8,5,3));
		aux.add(new Casilla(8,6,-1));
		aux.add(new Casilla(8,7,5));
		aux.add(new Casilla(8,8,3));
		aux.add(new Casilla(8,9,-1));
		
		aux.add(new Casilla(9,0,5));
		aux.add(new Casilla(9,1,6));
		aux.add(new Casilla(9,2,7));
		aux.add(new Casilla(9,3,6));
		aux.add(new Casilla(9,4,4));
		aux.add(new Casilla(9,5,4));
		aux.add(new Casilla(9,6,-1));
		aux.add(new Casilla(9,7,6));
		aux.add(new Casilla(9,8,4));
		aux.add(new Casilla(9,9,5));
		
		return aux;
	}

	/**
	 * Controla si puede avanzar a la posicion por questiones de tamanyo
	 * o si hay un precipicio
	 * @param x: coordenada horizontal
	 * @param i: coordenada vertical
	 * @return Cierto si puede avanzar y Falso si no
	 */
	public boolean canGo(int x, int i) {
		if(x>=this.rows || x<0 || i>=this.cols || i<0 || buscarPorCoordenada(x, i,this.tablero).isPrecipicio())
			return false;
		return true;
	}

	/**
	 * Dada una coordenada marca la casilla como recorrida
	 * @param x: fila
	 * @param y: columna
	 */
	public void marcaComoRecorrida(int x, int y) {
		buscarPorCoordenada(x,y,this.tablero).setRecorrido(true);	
	}
	
	/**
	 * Dada una coordenada marca la casilla como no recorrida
	 * @param x: fila
	 * @param y: columna
	 */
	public void marcaComoNoRecorrida(int x, int y) {
		buscarPorCoordenada(x,y,this.tablero).setRecorrido(false);	
	}
	
	/**
	 * Dada una coordenada devuelve el codigo de su casilla
	 * @param c: coordenada
	 * @return codigo de la casilla
	 */
	public int getCodigoCasilla(Casilla c) {
		return buscarPorCoordenada(c.getRow(),c.getCol(),this.tablero).getCodigo();	
	}
	
	/**
	 * Sigue el camino pasado desde la casilla inicial pasada por parametro
	 * y escribe el numero 666 en las casillas por las que pasa
	 * @param path: camino a seguir
	 * @param ini: casilla con coordenadas iniciales
	 * @return el conjunto de casillas con las casillas del camino
	 * marcadas con un codigo de 666
	 */
	public ArrayList<Casilla> marcarCamino(String path,Casilla ini) {
		String direcciones[]=path.split("-");
		ArrayList<Casilla> tab= cloneTablero(this.tablero);
		tab.get(ini.getRow()*cols+ini.getCol()).setCodigo(666);
		for(int i=0;i<direcciones.length;i++) {
			switch(direcciones[i]) {
				case("DOWN"):ini.setRow(ini.getRow()+1);break;
				case("UP"):ini.setRow(ini.getRow()-1);break;
				case("RIGHT"):ini.setCol(ini.getCol()+1);break;
				case("LEFT"):ini.setCol(ini.getCol()-1);break;
			}
			tab.get(ini.getRow()*cols+ini.getCol()).setCodigo(666);
		}
		return tab;
	}
	
	/**
	 * Busca una casilla del mapa y la devuelve por parametro
	 * @param row: numero de fila 
	 * @param col: numero de columna
	 * @param m: mapa
	 * @return Casilla encontrada
	 */
	public Casilla buscarPorCoordenada(int row, int col,ArrayList<Casilla> m) {
		return m.get(row*cols+col);
	}
	
	/**
	 * Genera un tablero igual al pasado por parametro
	 * y lo devuelve
	 * @param t: tablero a copiar
	 * @return copia de t
	 */
	public ArrayList<Casilla> cloneTablero(ArrayList<Casilla> t){
		ArrayList<Casilla> aux = new ArrayList<Casilla>();
		for (int i = 0; i < t.size(); i++) {
			aux.add(t.get(i).clone());
		}
		return aux;
		
	}
	
	/**
	 * Genera un tablero del tama�o del mapa con 
	 * codigos aleatorios de entre 1 y 20
	 * @return Tablero con codigo aleatorios
	 */
	public ArrayList<Casilla> generaTableroRandom(){
		ArrayList<Casilla> t= new ArrayList<Casilla>();
		for (int i = 0; i < this.rows; i++) {
			for (int j = 0; j < this.cols; j++) {
				t.add(new Casilla(i, j, (int) (Math.random()*19+1)));
			}
		}
		return t;
	}
	
	public ArrayList<Casilla> generaTableroPersonalizado(){
		ArrayList<Casilla>  t = generaTableroRandom();
		buscarPorCoordenada(0, 0, t).setCodigo(18);
		buscarPorCoordenada(0, 1, t).setCodigo(17);
		buscarPorCoordenada(0, 3, t).setCodigo(-1);
		buscarPorCoordenada(0, 7, t).setCodigo(-1);
		buscarPorCoordenada(1, 1, t).setCodigo(16);
		buscarPorCoordenada(1, 2, t).setCodigo(15);
		buscarPorCoordenada(1, 3, t).setCodigo(14);
		buscarPorCoordenada(1, 4, t).setCodigo(13);
		buscarPorCoordenada(1, 7, t).setCodigo(-1);
		buscarPorCoordenada(2, 3, t).setCodigo(-1);
		buscarPorCoordenada(2, 4, t).setCodigo(12);
		buscarPorCoordenada(2, 5, t).setCodigo(11);
		buscarPorCoordenada(2, 6, t).setCodigo(10);
		buscarPorCoordenada(2, 7, t).setCodigo(-1);
		buscarPorCoordenada(3, 3, t).setCodigo(-1);
		buscarPorCoordenada(3, 6, t).setCodigo(9);
		buscarPorCoordenada(3, 7, t).setCodigo(-1);
		buscarPorCoordenada(4, 3, t).setCodigo(-1);
		buscarPorCoordenada(4, 6, t).setCodigo(8);
		buscarPorCoordenada(5, 3, t).setCodigo(-1);
		buscarPorCoordenada(5, 6, t).setCodigo(7);
		buscarPorCoordenada(5, 7, t).setCodigo(-1);
		buscarPorCoordenada(6, 0, t).setCodigo(-1);
		buscarPorCoordenada(6, 2, t).setCodigo(-1);
		buscarPorCoordenada(6, 3, t).setCodigo(-1);
		buscarPorCoordenada(6, 6, t).setCodigo(6);
		buscarPorCoordenada(6, 7, t).setCodigo(5);
		buscarPorCoordenada(6, 8, t).setCodigo(4);
		buscarPorCoordenada(7, 7, t).setCodigo(-1);
		buscarPorCoordenada(7, 8, t).setCodigo(3);
		buscarPorCoordenada(8, 8, t).setCodigo(2);
		buscarPorCoordenada(9, 7, t).setCodigo(-1);
		buscarPorCoordenada(9, 8, t).setCodigo(1);
		buscarPorCoordenada(9, 9, t).setCodigo(0);
		return t;
	}

}
