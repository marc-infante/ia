package dto;

public class Operadores {
	
	
	/**
	 * Comprueba si puede hacer la operacion y en el caso de hacerlo
	 * devuelve la casilla con la informacion, sino la actual
	 * @param tren: casilla actual
	 * @param map: mapa usado
	 * @param op: operacion a realizar
	 * @return Una casilla con unas nuevas coordenadas o las mismas en caso de no poder ir
	 */
	public Casilla operacion(Casilla tren, Mapa map,String op) {
		Casilla c =tren.clone();
		int row = c.getRow();
		int col = c.getCol();
		boolean vertical=true;
		switch(op) {
		case ("DOWN"):
			row =c.getRow()+1; vertical= false; break;
		case ("UP"):
			row =c.getRow()-1; vertical= false; break;
		case ("RIGHT"):
			col = c.getCol()+1;break;
		case ("LEFT"):
			col = c.getCol()-1;break;
		}
		if(map.canGo(row,col) && !map.buscarPorCoordenada(row,col,map.getTablero()).isRecorrido()) {
			if(vertical)
				c.setCol(col);
			else
				c.setRow(row);
		}
		return c;
	}
	
	

}
