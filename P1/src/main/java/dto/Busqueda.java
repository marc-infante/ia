package dto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class Busqueda implements Constants {

	private Mapa map;
	private Operadores op;
	private String  heu_type;
	private Casilla tren;
	private Casilla inicio;
	private Casilla objetivo;
	private ArrayList<Casilla> pendientes;
	private ArrayList<Casilla> resultado;
	private int guardados;
	private int recorridos;
	private double optimTime;

	/**
	 * @param sizeX: tama�o horizontal del tablero
	 * @param sizeY: tama�o vertical del tablero
	 * @param inicio: coordenada inicial
	 * @param objetivo: coordenada final
	 * @param optimTime: tiempo optimo
	 */
	public Busqueda(int sizeX,int sizeY,Casilla inicio, Casilla objetivo, double optimTime) {
		this.map = new Mapa(sizeX,sizeY);
		this.op = new Operadores();
		this.heu_type = HEU_DIST;
		this.inicio = map.buscarPorCoordenada(inicio.getRow(), inicio.getCol(), map.getTablero());
		this.tren = this.inicio.clone();
		this.objetivo = objetivo;
		this.pendientes = new ArrayList<Casilla>();
		pendientes.add(tren.clone());
		this.guardados = 0;
		this.recorridos = 0;
		this.optimTime = optimTime;
		this.resultado = null;
	}

	
	/**
	 * @param m: mapa a usar
	 * @param heu: tipo de heuristica
	 * @param inicio: coordenada inicial
	 * @param objetivo: coordenada final
	 * @param optimTime: tiempo optimo para resolver el problema
	 */
	public Busqueda(Mapa m,String heu, Casilla inicio, Casilla objetivo, double optimTime) {
		this.map = new Mapa(m.getRows(),m.getCols(),m.cloneTablero(m.getTablero()));
		this.op = new Operadores();
		this.heu_type = heu;
		this.inicio = map.buscarPorCoordenada(inicio.getRow(), inicio.getCol(), map.getTablero());
		this.tren = this.inicio.clone();
		this.objetivo = objetivo;
		this.pendientes = new ArrayList<Casilla>();
		pendientes.add(tren.clone());
		this.guardados = 0;
		this.recorridos = 0;
		this.optimTime = optimTime;
		this.resultado = null;
	}
	
	
	/**
	 * @return the map
	 */
	public Mapa getMap() {
		return map;
	}


	/**
	 * @param map the map to set
	 */
	public void setMap(Mapa map) {
		this.map = map;
	}


	/**
	 * @return the op
	 */
	public Operadores getOp() {
		return op;
	}


	/**
	 * @param op the op to set
	 */
	public void setOp(Operadores op) {
		this.op = op;
	}


	/**
	 * @return the tren
	 */
	public Casilla getTren() {
		return tren;
	}


	/**
	 * @param tren the tren to set
	 */
	public void setTren(Casilla tren) {
		this.tren = tren;
	}


	/**
	 * @return the objetivo
	 */
	public Casilla getObjetivo() {
		return objetivo;
	}


	/**
	 * @param objetivo the objetivo to set
	 */
	public void setObjetivo(Casilla objetivo) {
		this.objetivo = objetivo;
	}


	/**
	 * @return the pendientes
	 */
	public ArrayList<Casilla> getPendientes() {
		return pendientes;
	}


	/**
	 * @param pendientes the pendientes to set
	 */
	public void setPendientes(ArrayList<Casilla> pendientes) {
		this.pendientes = pendientes;
	}


	/**
	 * @return the guardados
	 */
	public int getGuardados() {
		return guardados;
	}


	/**
	 * @param guardados the guardados to set
	 */
	public void setGuardados(int guardados) {
		this.guardados = guardados;
	}
	
	/**
	 * @return the inicio
	 */
	public Casilla getInicio() {
		return inicio;
	}


	/**
	 * @param inicio the inicio to set
	 */
	public void setInicio(Casilla inicio) {
		this.inicio = inicio;
	}


	/**
	 * @return the recorridos
	 */
	public int getRecorridos() {
		return recorridos;
	}


	/**
	 * @param recorridos the recorridos to set
	 */
	public void setRecorridos(int recorridos) {
		this.recorridos = recorridos;
	}

	
	/**
	 * @return the optimTime
	 */
	public double getOptimTime() {
		return optimTime;
	}


	/**
	 * Ejecuta la busqueda del camino y devuelve la coordenada
	 * de la casilla soluci�n
	 * @param bestFirst: true para bestFirst y false para A* 
	 * @return Los resultados de la prueba
	 */
	public String ejecutaBusqueda(boolean bestFirst) {
		boolean found= false;
		try {
		while (!found && pendientes.size()!=0) {
			this.tren = pendientes.get(0).clone();
			pendientes.remove(0);
			map.marcaComoRecorrida(this.tren.getRow(),this.tren.getCol());
			recorridos++;
			realizaOperaciones(bestFirst);
			if( pendientes.get(0).equals(objetivo)) {
				found= true;
			}
		}
		this.tren = pendientes.get(0).clone();
		this.resultado=map.marcarCamino(this.tren.getPath(), this.inicio.clone());
		return muestraResultados(this.tren);
		}catch(IndexOutOfBoundsException e ) {
			return ("NO EXISTE UN CAMINO POSIBLE");
		}
	}

	/**
	 * Realiza las operaciones posibles y a�ade a la lista de pendientes.
	 * Para las coordenadas que puede a�adir genera su valor heur�stico
	 * y el camino usado para llegar hasta alli, ademas de marcar la casilla 
	 * como recorrida. Finalmente ordena la lista.
	 * @param bestFirst: true para bestFirst y false para A*
	 */
	private void realizaOperaciones(boolean bestFirst) {
		Casilla aux;
		for (int i = 0; i < NUM_OP; i++) {
			aux = new Casilla();
			aux = op.operacion(this.tren, this.map,OPERACION[i]);
			if (!aux.equals(this.tren) && !map.buscarPorCoordenada(aux.getRow(), aux.getCol(),map.getTablero()).isPrecipicio()) {
				addNode(aux.clone(), OPERACION[i]+DEL,bestFirst);
			}
		}
		this.ordenaLista();
	}
	
	/**
	 * A�ade un nodo a la lista de pendientes 
	 * @param aux: coordenada de la casilla a a�adir
	 * @param camino: string de la operacion descrita
	 * @param bestFirst: true para bestFirst y false para A* 
	 */
	private void addNode(Casilla aux,String camino,boolean bestFirst) {
		aux.setValHeu(funcionHeuristica(aux));
		aux.setPath(this.tren.getPath()+camino);
		aux.setTime(tren.getTime()+calculaTiempo(aux));
		if(bestFirst) {
			if(!existInPendientes(aux)) {
				pendientes.add(aux.clone());
				guardados++;
			}
		}
		else {
			pendientes.add(aux.clone());
			guardados++;
		}
		
	}

	/**
	 * A�ade un nodo a la lista de pendientes, en el
	 * caso de estar repetido mira cual tiene mejor heuristica
	 * y se queda con el mejor
	 * @param c: casilla nueva
	 */
	private boolean existInPendientes(Casilla c) {
		Casilla aux;
		boolean found =false;
		for(int i=0; i < pendientes.size() && !found; i++) {
			aux=pendientes.get(i);
			if(aux.equals(c)) {
				found = true;
			}
		}
		return found;
		
	}

	/**
	 * Devuelve el valor heuristico dependiendo de la heuristica que se este usando
	 * @param destino: casilla a la que queremos pasar 
	 * @return valor heuristico
	 */
	private double funcionHeuristica(Casilla destino) {
		switch(this.heu_type) {
			case HEU_DIST :return calculaDistancia(destino);
			case HEU_TIME :return calculaTiempo(destino)+this.tren.getValHeu();
			case HEU_DIST_TIME: return calculaTiempo(destino)+calculaDistancia(destino)+(Math.abs(calculaDiferenciaNegativa(destino))*0.1);
			default: return -1;
		}
		
	}

	/**
	 * Calcula el tiempo desde la casilla actual hasta la
	 * pasada por parametro
	 * @param destino: casilla final
	 * @return tiempo total hasta llegar alli
	 */
	private double calculaTiempo(Casilla destino) {
		double tiempo = 1;
		int diff= map.getCodigoCasilla(destino)-map.getCodigoCasilla(this.tren);
		if(diff>=0) 
			tiempo += diff;
		else
			tiempo -= 0.5;
		return tiempo;
	}
	
	/**
	 * Calcula la diferencia entre los codigos de la 
	 * casilla actual y a la que se quiere pasar y si la
	 * diferencia es negativa, suma 0,1 por cada unidad de diferencia
	 * existente
	 * @param destino: casilla a la que se quiere pasar
	 * @return proporcion de diferencia negativa
	 */
	private double calculaDiferenciaNegativa(Casilla destino) {
		int diff = map.getCodigoCasilla(destino)-map.getCodigoCasilla(this.tren);
		if(diff<0) {
			return (Math.abs(diff)*0.1);
		}
		return 0;
	}
	/**
	 * Calcula la distancia desde la casilla pasada por parametro
	 * hasta el objetivo
	 * @param destino: casilla desde la que medir
	 * @return distancia entre las dos casillas
	 */
	private int calculaDistancia(Casilla destino) {
		return Math.abs(this.objetivo.getRow()-destino.getRow())+Math.abs(this.objetivo.getCol()-destino.getCol());
	}

	/**
	 * Ordena la lista de pendientes de forma ascendente segun
	 * los valores heur�sticos de las coordenadas
	 */
	public void ordenaLista() {
		Collections.sort(pendientes, new Comparator<Casilla>(){

			  public int compare(Casilla o1, Casilla o2)
			  {
			     return o1.compareTo(o2.getValHeu());
			  }
			});
	}
	
	/**
	 * Genera un String con los estados pendientes
	 * @return El string con la info
	 */
	public String muestraPendientes() {
		return "Busqueda [pendientes=" + pendientes + "]";
	}
	
	/**
	 * Dada la coordenada final con la informacion
	 * devuelve un String con los resultados
	 * @param fin: coordenada final
	 * @return String con la informacion final
	 */
	public String muestraResultados(Casilla fin) {
		String result="**************************************************************************************************" + 
						"\n" + this.heu_type + 
						"\nTiempo usado: "+ fin.getTime() + 
						"\nNodos guardados: "+ this.guardados +
						"\nNodos recorridos: "+ this.recorridos;
		if(optimTime(fin.getTime())) {
			result+="\nSoluci�n OPTIMA";
		}else
			result+="\nSoluci�n NO OPTIMA";
		result+="\n\nCamino escogido:\n"+map.muestraTablero(this.resultado,this.map.getRows());
		result+="\n**************************************************************************************************";
		
		return result;
	}

	/**
	 * Devuelve si el tiempo es igual al optimo o no
	 * @param time: tiempo a comparar
	 * @return cierto si el tiempo es optimo y falso si no
	 */
	private boolean optimTime(double time) {
		if (time ==this.optimTime)
			return true;
		return false;
	}
	
	/**
	 * Reestablece los datos a los del inicio de la prueba
	 */
	public void reestablecer() {
		this.tren = this.inicio.clone();
		this.pendientes = new ArrayList<Casilla>();
		pendientes.add(tren);
		this.resultado = new ArrayList<Casilla>();
		this.recorridos = 0;
		this.guardados = 0;
		for (int i = 0; i < map.getTablero().size(); i++) {
			map.marcaComoNoRecorrida(map.getTablero().get(i).getRow(), map.getTablero().get(i).getCol());
		}
	}
	
	
	
}
