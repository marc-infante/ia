package dto;

public class Casilla {
	
	private int row;
	private int col;
	private double valHeu;
	private String path;
	private double time;
	private int codigo;
	private boolean recorrido;
	
	public Casilla() {
		this.row=0;
		this.col=0;
		this.valHeu=0;
		this.path="";
		this.time=0;
		this.codigo = -1;
		this.recorrido =false;
	}

	/**
	 * @param x: fila
	 * @param y: columna
	 */
	public Casilla(int x, int y) {
		this.row = x;
		this.col = y;
		this.valHeu = 0;
		this.path="";
		this.time=0;
		this.codigo = -1;
		this.recorrido =false;
	}
	
	/**
	 * @param x: fila 
	 * @param y: columna
	 * @param codigo: altura de la casilla
	 */
	public Casilla(int x, int y, int codigo) {
		this.row = x;
		this.col = y;
		this.valHeu = 0;
		this.path="";
		this.time=0;
		this.codigo = codigo;
		this.recorrido =false;
	}
	
	

	/**
	 * @param x: fila
	 * @param y: columna
	 * @param valHeu: valor heuristico de la casilla
	 * @param codigo: altura de la casilla
	 */
	public Casilla(int x, int y, double valHeu,int codigo) {
		super();
		this.row = x;
		this.col = y;
		this.valHeu = valHeu;
		this.path="";
		this.time=0;
		this.codigo = codigo;
		this.recorrido =false;
	}

	/**
	 * @return the x
	 */
	public int getRow() {
		return row;
	}

	/**
	 * @param x the x to set
	 */
	public void setRow(int x) {
		this.row = x;
	}

	/**
	 * @return the y
	 */
	public int getCol() {
		return col;
	}

	/**
	 * @param y the y to set
	 */
	public void setCol(int y) {
		this.col = y;
	}
	/**
	 * @return the valHeu
	 */
	public double getValHeu() {
		return valHeu;
	}

	/**
	 * @param valHeu the valHeu to set
	 */
	public void setValHeu(double valHeu) {
		this.valHeu = valHeu;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}
	
	/**
	 * @return the time
	 */
	public double getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(double time) {
		this.time = time;
	}

	/**
	 * @return the codigo
	 */
	public int getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the recorrido
	 */
	public boolean isRecorrido() {
		return recorrido;
	}

	/**
	 * @param recorrido the recorrido to set
	 */
	public void setRecorrido(boolean recorrido) {
		this.recorrido = recorrido;
	}

	public String muestraCoordenada() {
		return row + ","+ col;
	}
	
	@Override
	public String toString() {
		return "Coordenada:"
				+ "\n[x=" + row + ","
				+ "\ny=" + col + ","
				+ "\nvalHeu=" + valHeu + ","
				+ "\npath=" + path + ","
				+ "\ntiempo=" + time + ","
				+ "\ncodigo=" + codigo + ","
				+ "\nrecorrido=" + recorrido + "]";
	}
	
	public Casilla clone() {
		Casilla aux = new Casilla();
		aux.setRow(this.row);
		aux.setCol(this.col);
		aux.setPath(this.path);
		aux.setValHeu(this.valHeu);
		aux.setTime(this.time);
		aux.setCodigo(this.codigo);
		aux.setRecorrido(this.recorrido);
		return aux;
	}

	@Override
	public boolean equals(Object obj) {
		Casilla other = (Casilla) obj;
		if (row != other.row)
			return false;
		if (col != other.col)
			return false;
		return true;
	}
	
	public int compareTo(double val) {
		if(this.valHeu>=val)
			return 1;
		else
			return -1;
		
	}
	
	/**
	 * Devuelve si la casilla es precipicio o no
	 * @return Cierto si es precipicio y Falso si no
	 */
	public boolean isPrecipicio() {
		if(codigo==-1)
			return true;
		return false;
	}
}
