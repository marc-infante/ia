package dto;

public interface Constants {

	public final String HEU_DIST="Heuristica distancia hasta objetivo";
	public final String HEU_TIME="Heuristica tiempo acumulado";
	public final String HEU_DIST_TIME="Heuristica distancia hasta objetivo + tiempo hasta siguiente casilla";
	public final String[] OPERACION= {"DOWN","UP","LEFT","RIGHT"};
	public final String DEL="-";
	public final int NUM_OP=4;
}
