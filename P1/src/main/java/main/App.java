package main;


import java.util.ArrayList;
import java.util.List;

import dto.Busqueda;
import dto.Casilla;
import dto.Constants;
import dto.Mapa;

public class App {
	
	private static void mapa(String title,Mapa m) {
		System.out.println("-----------------------------------------------------------------------------\n"+
				title+"\n\n"+m.muestraTablero(m.getTablero(), m.getRows())+
								   "\n-----------------------------------------------------------------------------\n");
	}
	
	private static void doSearches(List<Busqueda> searches) {
		for (Busqueda busqueda : searches) {
			muestraResult("---BEST FIRST---", busqueda.ejecutaBusqueda(true));
			busqueda.reestablecer();
			muestraResult("---A*---", busqueda.ejecutaBusqueda(false));
		}
	}
	
	private static void muestraResult(String type,String result) {
		System.out.println("\n\n\n"+type+"\n"+result);
	}

	public static void main(String[] args) {
		
		// PRUEBAS CON EL MAPA PROPUESTO EN EL ENUNCIADO
		
		Mapa m= new Mapa();
		m.generaDefault();
		Casilla objetivo = new Casilla(9, 9);
		Casilla inicio = new Casilla(0, 0);
		double optimTime = 30;
		List<Busqueda> searches=  new ArrayList<Busqueda>();
		searches.add(new Busqueda(m, Constants.HEU_DIST, inicio, objetivo,optimTime));
		searches.add(new Busqueda(m, Constants.HEU_TIME, inicio, objetivo,optimTime));
		searches.add(new Busqueda(m, Constants.HEU_DIST_TIME, inicio, objetivo,optimTime));
		
		//CON MAPA DEFAULT
		
		mapa("TABLERO DEFAULT",m);
		doSearches(searches);
		
		
		//CON MAPA PROPIO
		m.setTablero(m.generaTableroPersonalizado());
		searches.clear();
		optimTime = 9;
		searches.add(new Busqueda(m, Constants.HEU_DIST, inicio, objetivo,optimTime));
		searches.add(new Busqueda(m, Constants.HEU_TIME, inicio, objetivo,optimTime));
		searches.add(new Busqueda(m, Constants.HEU_DIST_TIME, inicio, objetivo,optimTime));
		
		mapa("TABLERO PERSONALIZADO",m);
		doSearches(searches);
	}

}
