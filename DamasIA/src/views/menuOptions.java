package views;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


import javax.swing.JLabel;
import javax.swing.JRadioButton;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class menuOptions extends JFrame {
	
	private static final long serialVersionUID = -1014980864466811634L;
	
	private JLabel typeGameLabel,colorOptionLabel,lblHeu,lblAlg;
	private JRadioButton pcVsPcButton,pcVsHumanButton;
	private JRadioButton blackButton,whiteButton;
	private ButtonGroup op1,op2;
	private JPanel contentPane;
	private JComboBox<String> heu_pc1,heu_pc2,alg_pc1,alg_pc2; 
	
	public menuOptions() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 654, 457);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		op1 = new ButtonGroup();
		op2 = new ButtonGroup();
		
		typeGameLabel = new JLabel("Tipo de juego:");
		typeGameLabel.setFont(new Font("Yu Gothic Medium", Font.PLAIN, 17));
		typeGameLabel.setBounds(25, 35, 140, 37);
		contentPane.add(typeGameLabel);
		
		pcVsPcButton = new JRadioButton("PC VS PC");
		pcVsPcButton.setActionCommand("pcVsPcButton");
		pcVsPcButton.setFont(new Font("Yu Gothic Medium", Font.PLAIN, 17));
		pcVsPcButton.setSelected(true);
		pcVsPcButton.setBounds(35, 78, 176, 44);
		pcVsPcButton.addActionListener(typeGameListener);
		contentPane.add(pcVsPcButton);
		
		pcVsHumanButton = new JRadioButton("PC VS HUMANO");
		pcVsHumanButton.setActionCommand("pcVsHumanButton");
		pcVsHumanButton.setFont(new Font("Yu Gothic Medium", Font.PLAIN, 17));
		pcVsHumanButton.setBounds(35, 128, 176, 53);
		pcVsHumanButton.addActionListener(typeGameListener);
		contentPane.add(pcVsHumanButton);
		
		op1.add(pcVsPcButton);op1.add(pcVsHumanButton);
		
		colorOptionLabel = new JLabel("Elija su color:");
		colorOptionLabel.setFont(new Font("Yu Gothic Medium", Font.PLAIN, 17));
		colorOptionLabel.setBounds(253, 35, 140, 37);
		contentPane.add(colorOptionLabel);
		
		
		blackButton = new JRadioButton("NEGRAS");
		blackButton.setFont(new Font("Yu Gothic Medium", Font.PLAIN, 17));
		blackButton.setBounds(263, 78, 103, 37);
		contentPane.add(blackButton);
		
		whiteButton = new JRadioButton("BLANCAS");
		whiteButton.setSelected(true);
		whiteButton.setFont(new Font("Yu Gothic Medium", Font.PLAIN, 17));
		whiteButton.setBounds(263, 128, 122, 37);
		contentPane.add(whiteButton);
		
		op2.add(blackButton);op2.add(whiteButton);
		hideColorOptions();
		
		JButton btnNewButton = new JButton("Nueva Partida");
		btnNewButton.setBackground(Color.CYAN);
		btnNewButton.setFont(new Font("Rockwell", Font.PLAIN, 16));
		btnNewButton.setBounds(445, 322, 150, 58);
		btnNewButton.addActionListener(newGame);
		contentPane.add(btnNewButton);
		
		heu_pc1 = new JComboBox<String>();
		heu_pc1.setModel(new DefaultComboBoxModel<String>(new String[] {"Estrategico", "Defensivo", "Ofensivo"}));
		heu_pc1.setSelectedIndex(0);
		heu_pc1.setBounds(35, 222, 130, 21);
		contentPane.add(heu_pc1);
		
		heu_pc2 = new JComboBox<String>();
		heu_pc2.setModel(new DefaultComboBoxModel<String>(new String[] {"Estrategico", "Defensivo", "Ofensivo"}));
		heu_pc2.setSelectedIndex(0);
		heu_pc2.setBounds(35, 324, 130, 21);
		contentPane.add(heu_pc2);
		
		alg_pc1 = new JComboBox<String>();
		alg_pc1.setModel(new DefaultComboBoxModel<String>(new String[] {"Minimax", "Poda alfa-beta"}));
		alg_pc1.setSelectedIndex(0);
		alg_pc1.setBounds(253, 222, 113, 21);
		contentPane.add(alg_pc1);
		
		alg_pc2 = new JComboBox<String>();
		alg_pc2.setModel(new DefaultComboBoxModel<String>(new String[] {"Minimax", "Poda alfa-beta"}));
		alg_pc2.setSelectedIndex(0);
		alg_pc2.setBounds(253, 324, 113, 21);
		contentPane.add(alg_pc2);
		
		JLabel lblNewLabel = new JLabel("Heuristica PC1");
		lblNewLabel.setFont(new Font("Yu Gothic Medium", Font.PLAIN, 17));
		lblNewLabel.setBounds(25, 184, 138, 28);
		contentPane.add(lblNewLabel);
		
		lblHeu = new JLabel("Heuristica PC2");
		lblHeu.setFont(new Font("Yu Gothic Medium", Font.PLAIN, 17));
		lblHeu.setBounds(25, 286, 138, 28);
		contentPane.add(lblHeu);
		
		JLabel lblAlgoritmoPc = new JLabel("Algoritmo PC1");
		lblAlgoritmoPc.setFont(new Font("Yu Gothic Medium", Font.PLAIN, 17));
		lblAlgoritmoPc.setBounds(235, 184, 138, 28);
		contentPane.add(lblAlgoritmoPc);
		
		lblAlg = new JLabel("Algoritmo PC2");
		lblAlg.setFont(new Font("Yu Gothic Medium", Font.PLAIN, 17));
		lblAlg.setBounds(235, 286, 138, 28);
		contentPane.add(lblAlg);
	}
	
	ActionListener typeGameListener =  new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getActionCommand().equals("pcVsPcButton")) {
				hideColorOptions();
				showPC2Options();
			}else {
				showColorOptions();
				hidePC2Options();
			}
			
		}
	};
	
	ActionListener newGame = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			int heu1=heu_pc1.getSelectedIndex();
			int alg1=alg_pc1.getSelectedIndex();
			int heu2=heu_pc2.getSelectedIndex();
			int alg2=alg_pc2.getSelectedIndex();
			if(op1.getSelection().equals(pcVsHumanButton.getModel())) {
				if(op2.getSelection().equals(whiteButton.getModel())){
					new checkersGame("w",heu1,alg1,heu2,alg2);	
				}else {
					new checkersGame("b",heu1,alg1,heu2,alg2);
				}
			}else {
				new checkersGame(null,heu1,alg1,heu2,alg2);
			}
			dispose();
			
		}
	};
	
	
	private void hideColorOptions() {
		colorOptionLabel.setVisible(false);
		blackButton.setVisible(false);
		whiteButton.setVisible(false);
	}
	
	private void showColorOptions() {
		colorOptionLabel.setVisible(true);
		blackButton.setVisible(true);
		whiteButton.setVisible(true);
	}
	
	private void hidePC2Options() {
		lblHeu.setVisible(false);
		lblAlg.setVisible(false);
		heu_pc2.setVisible(false);
		alg_pc2.setVisible(false);
	}
	
	private void showPC2Options() {
		lblHeu.setVisible(true);
		lblAlg.setVisible(true);
		heu_pc2.setVisible(true);
		alg_pc2.setVisible(true);
	}
}
