package views;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import obj.Coordinate;
import obj.CoordinatePair;
import structures.Board;
import structures.Tree;

import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;

public class checkersGame extends JFrame{
	
	private static final long serialVersionUID = 1994125118282326382L;
	
	private final Color WHITE = new Color(213,198,186);
	private final Color BLACK = new Color(156,74,32);
	private final Color BLACK_SELECTED = BLACK.brighter();
	private final String LOCATION_IMG = "C:/Users/jimmy/Desktop/IA/Archivos";
	private final String TURN_W ="TIRAN LAS BLANCAS";
	private final String TURN_N ="TIRAN LAS NEGRAS";
	
	
	private boolean whiteWithoutMoves=false,blackWithoutMoves=false,colorWin=false;
	private Board boardImpl =  new Board();
	private String[] colours = {boardImpl.BLACK,boardImpl.WHITE};
	private Icon white,black;
	private ArrayList<JButton> boardView = new ArrayList<JButton>();
	private JPanel contentPane;
	private JLabel message;
	private boolean selected = false; //Detect when a piece is selected
	private CoordinatePair options = new CoordinatePair(-1, -1, -1, -1);//options selected
	private Coordinate selectedButton = new Coordinate(0, 0);//button selected
	private String player = null;
	private Tree t = new Tree();
	private int turn=0,heu1,alg1,heu2,alg2;

	/**
	 * Constructor
	 * @param player: white, black or null
	 */
	public checkersGame(String player,int heu1,int alg1,int heu2,int alg2) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 850, 550);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setFocusable(true);
		contentPane.addKeyListener(space);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		
		this.player = player;
		this.heu1=heu1;
		this.heu2=heu2;
		this.alg1=alg1;
		this.alg2=alg2;
		
		JPanel board = new JPanel();
		board.setLayout(new GridLayout(0, 8, 0, 0));
		contentPane.add(board);
		JPanel info = new JPanel();
		contentPane.add(info);
		info.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel title = new JLabel("CHECKERS");
		title.setVerticalAlignment(SwingConstants.TOP);
		title.setHorizontalAlignment(SwingConstants.CENTER);
		title.setFont(new Font("Sitka Text", Font.BOLD, 16));
		info.add(title);
		
		message = new JLabel(TURN_N);
		message.setHorizontalAlignment(SwingConstants.CENTER);
		info.add(message);
		
		
		
		//INITIALIZE BUTTONS
		this.white = new ImageIcon(resizeImgToButton(LOCATION_IMG+"/white.png")); 
		this.black = new ImageIcon(resizeImgToButton(LOCATION_IMG+"/black.png")); 
		for (int i = 0; i < boardImpl.SIZE; i++) {
			for (int j = 0; j < boardImpl.SIZE; j++) {
				boardView.add(new JButton());
				getJButton(i, j).addActionListener(clickButton);
				getJButton(i, j).setName(new Coordinate(i, j).toString());
				getJButton(i, j).addKeyListener(space);
				getJButton(i, j).setEnabled(true);
				getJButton(i, j).setFont(new Font("Snap IPC",Font.BOLD , 35));
				board.add(getJButton(i, j));
				if((i+j)%2!=0) {
					getJButton(i, j).setBackground(BLACK);
					if(i<3) 
						setIconTo(getJButton(i, j),boardImpl.BLACK);
					if(i>4) 
						setIconTo(getJButton(i, j),boardImpl.WHITE);
				}else
					getJButton(i, j).setBackground(WHITE);
			}
		
		}
		this.setVisible(true);
	}
	
	/**
	 * Check if the player is white, black or null
	 * @param s: color to check
	 * @return true if the color is equal to player or false
	 */
	public boolean isPlayer(String s) {
		if(player == null) {
			if(s == null)
				return true;
		}else if(player.equals(s)) {
			return true;
		}
		return false;
	}
	
	/**
	 * @param boardImpl the boardImpl to set
	 */
	public void setBoardImpl(Board boardImpl) {
		this.boardImpl = boardImpl;
	}
	
	/**
	 * Given the row and the column of the board
	 * return the button that contain his information
	 * @param i: row
	 * @param j: column
	 * @return the button in the position searched
	 */
	private JButton getJButton(int i, int j) {
		return boardView.get(i*boardImpl.SIZE + j);
	}
	
	/**
	 * Set the icon of the given button to null
	 * @param box: button 
	 */
	private void setIconToNull(JButton box) {
		box.setIcon(null);
	}
	
	/**
	 * Change the text of the frame label that show
	 * the turn
	 */
	private void setLabelTurn() {
		if(turn==0)
			message.setText(TURN_N);
		else
			message.setText(TURN_W);
	}
	
	/**
	 * Change the icon of the given button
	 * to the dama of the colour passed as parameter
	 * @param box: button
	 * @param color: colour of the piece
	 */
	private void setIconTo(JButton box,String color) {
		if(color.equals(boardImpl.WHITE))
			box.setIcon(white);
		else
			box.setIcon(black);
	}
	
	/**
	 * Set the colour background of the given button
	 * to mark as possible movement
	 * @param b: button
	 */
	private void setBackgroundToSelected(JButton b) {
		b.setBackground(BLACK_SELECTED);
	}
	
	/**
	 * Return the normal colour to the buttons
	 * that are detected as selected
	 */
	private void returnBoardColor() {
		Coordinate first= options.getFirstPair();
		Coordinate second= options.getSecondPair();
		if(boardImpl.isCoordinateValid(first))
			getJButton(first.getRow(), first.getCol()).setBackground(BLACK);
		if(boardImpl.isCoordinateValid(second))
			getJButton(second.getRow(), second.getCol()).setBackground(BLACK);
	}
	
	
	/**
	 * Resize an image to button size
	 * @param pathname: pathname of the location of the file
	 * @return the same image resized
	 */
	private Image resizeImgToButton(String pathname) {
		Image resizedImage = null;
		try {
			Image img = ImageIO.read(new File(pathname));
			resizedImage = 
				      img.getScaledInstance(50, 50, 5);
		} catch (IOException e) {
			return null;

		}
		return resizedImage;
		
	}
	
	/**
	 * Change the colour of the buttons that are 
	 * possible movements of the dama selected
	 * @param actual: button selected
	 * @param color: colour of the actual player
	 */
	private void paintSelected(Coordinate actual,String color) {
		Coordinate next;
		next = boardImpl.move(actual,false,color,boardImpl.MOVE_LEFT);
		if(next!=null) {
			setBackgroundToSelected(getJButton(next.getRow(),next.getCol()));
			options.setFirstPair(next);
			selected = true;
		}
		next = boardImpl.move(actual,false,color,boardImpl.MOVE_RIGHT);
		if(next!=null) {
			setBackgroundToSelected(getJButton(next.getRow(),next.getCol()));
			options.setSecondPair(next);
			selected = true;
		}
		
	}
	
	/**
	 * Given two coordinates and the colour of the player
	 * do the move in the board
	 * @param actual: coordinate to remove
	 * @param next: coordinate to create
	 * @param colorActualPlayer: colour of the actual player
	 */
	private void moveDama(Coordinate actual,Coordinate next,String colorActualPlayer) {
		Coordinate aux;
		setIconToNull(getJButton(actual.getRow(), actual.getCol()));
		setIconTo(getJButton(next.getRow(), next.getCol()),colorActualPlayer);
		aux = boardImpl.detectKill(next, actual);
		if(aux!=null) 
			setIconToNull(getJButton(aux.getRow(), aux.getCol()));
		boardImpl.moveDama(actual, next,colorActualPlayer);
		
	}
	
	
ActionListener clickButton= new ActionListener() {
		
		public void actionPerformed(ActionEvent e) {
			JButton button= (JButton) e.getSource();
			Coordinate actual = new Coordinate().valueOf(button.getName());
			Color background = button.getBackground();
			if(selected) {
				returnBoardColor();
				selected = false;
			}
			if(isPlayer(colours[turn])) {
				if(boardImpl.isMyDama(actual,colours[turn])) {
					paintSelected(actual, colours[turn]);
				}else if (background.equals(BLACK_SELECTED)) {
					moveDama(selectedButton,actual, colours[turn]);
					controlFinish();
				}
			}
			selectedButton = actual;
			
		}
		
	};
	
/**
 * When the key space is pressed the CPU play
 * his turn if can
 */
KeyListener space = new KeyListener() {
	@Override
	public void keyTyped(KeyEvent e) {}
	@Override
	public void keyReleased(KeyEvent e) {}
	@Override
	public void keyPressed(KeyEvent e) {
		if(player==null)
			CPUTurn();
		else if(turn==0 && player.equals(colours[1]))
			CPUTurn();
		else if(turn==1 && player.equals(colours[0]))
			CPUTurn();
	}
};

/**
 * Do the turn of the computer
 */
public void CPUTurn(){
	CoordinatePair aux;
	String c = colours[turn];
	aux=selectAlgorithm(selectAlgorithm(), c);
	if(aux!=null) {													//SI HAY MOVIMIENTO
		moveDama(aux.getFirstPair(), aux.getSecondPair(), c);		//MOVEMOS
		if(boardImpl.winColor() !=null)								//COMPROBAMOS QUE NO GANE
			colorWin = true;
		changeWithoutMovement(c, false);							//COMO HA MOVIDO MARCAMOS QUE SI PUEDE MOVER
		
	}else {													
		changeWithoutMovement(c, true);	
	}
	controlFinish();
}

/**
 * Given the option selected do one of the two
 * algorithms
 * @param op:option for select the algorithm
 * @param colour: colour of the actual player
 * @return the movement to do
 */
public CoordinatePair selectAlgorithm(int op,String colour) {
	if(op==0) 
		return t.minimax(colour, boardImpl.clone(),selectHeuristic());
	else
		return t.podaAlfaBeta(colour, boardImpl.clone(),selectHeuristic());
}

/**
 * Check which pc player is doing his turn
 * and return his algorithm option
 * @return the algorithm option
 */
public int selectAlgorithm() {
	if(player==null) {
		if(turn==0)
			return alg1;
		else
			return alg2;
	}else
		return alg1;
}

/**
 * Check which pc player is doing his turn
 * and return his heuristic option
 * @return the heuristic option 
 */
public int selectHeuristic() {
	if(player==null) {
		if(turn==0)
			return heu1;
		else
			return heu2;
	}else
		return heu1;
}

/**
 * Control if is the finish of the game
 * @return true if is the finish or false if not
 */
public boolean isFinish() {
	if(colorWin || (blackWithoutMoves && whiteWithoutMoves))
		return true;
	return false;
}

/**
 * Change the turn variable
 */
public void changeTurn() {
	if(turn==0)
		turn++;
	else
		turn--;	
}

/**
 * Give the number of the other turn
 * @return the other turn
 */
public int getOtherTurn() {
	if(turn==0)
		return 1;
	return 0;
}

/**
 * Given a colour and a boolean, changes his
 * boolean variable to mark when a player
 * doesn't have movements
 * @param colour: colour of the player to change
 * @param exist: boolean to assign at variable
 */
public void changeWithoutMovement(String colour,boolean exist) {
	if(colour.equals(colours[0]))
		blackWithoutMoves=exist;
	else
		whiteWithoutMoves=exist;
}

/**
 * Check that the other player has moves and is not the 
 * finish of the game, if it is block the frame and show a 
 * message with the winner
 */
public void controlFinish() {
	if(boardImpl.movesPossibles(colours[getOtherTurn()])!=null) {  //CAMBIAR DE TURNO SI EL OTRO JUGADOR TIENE MOVIMIENTOS
			changeTurn();
			setLabelTurn();
			changeWithoutMovement(colours[getOtherTurn()], false);
	}else {
		changeWithoutMovement(colours[getOtherTurn()], true);
		if(boardImpl.movesPossibles(colours[turn])==null)
			changeWithoutMovement(colours[turn], true);
	}
	if(isFinish())
		blockFrame(boardImpl.finishGame(boardImpl.clone()));
	
	
	
}

/**
 * Remove the action and key listeners of the frame
 */
public void blockFrame(String text) {
	message.setText(text);
	contentPane.removeKeyListener(space);
	for (JButton jButton : boardView) {
		jButton.removeKeyListener(space);
		jButton.removeActionListener(clickButton);
	}
}
}
