package obj;

public class Dama {
	
	private Coordinate position;
	private Boolean isDama;

	/**
	 * Create a men with a pair of coordinates
	 * @param row: initial row
	 * @param col: initial column
	 */
	public Dama(int row, int col) {
		this.position = new Coordinate(row, col);
		this.isDama=false;
	}

	/**
	 * @return the position
	 */
	public Coordinate getPosition() {
		return position;
	}

	/**
	 * @param position the position to set
	 */
	public void setPosition(Coordinate position) {
		this.position = position;
	}

	/**
	 * @return the isDama
	 */
	public Boolean getIsDama() {
		return isDama;
	}

	/**
	 * @param isDama the isDama to set
	 */
	public void setIsDama(Boolean isDama) {
		this.isDama = isDama;
	}
	
	

	
	public boolean equals(Dama obj) {
		if (position == null) {
			if (obj.position != null)
				return false;
		} else if (!position.equals(obj.position))
			return false;
		return true;
	}

	public Dama clone() {
		Dama aux = new Dama(this.position.getRow(), this.position.getCol());
		aux.setIsDama(this.isDama);
		return aux;
	}
	
}
