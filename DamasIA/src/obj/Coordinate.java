package obj;

public class Coordinate {
	private int row;
	private int col;
	
	
	
	/**
	 * 
	 */
	public Coordinate() {
	}
	/**
	 * @param row
	 * @param col
	 */
	public Coordinate(int row, int col) {
		super();
		this.row = row;
		this.col = col;
	}
	/**
	 * @return the row
	 */
	public int getRow() {
		return row;
	}
	/**
	 * @param row the row to set
	 */
	public void setRow(int row) {
		this.row = row;
	}
	/**
	 * @return the col
	 */
	public int getCol() {
		return col;
	}
	/**
	 * @param col the col to set
	 */
	public void setCol(int col) {
		this.col = col;
	}
	
	/**
	 * Create a new instance with a copy
	 * of this
	 */
	public Coordinate clone() {
		Coordinate aux = new Coordinate(this.row,this.col);
		return aux;
	}
	
	@Override
	public String toString() {
		return row + "," + col;
	}
	
	@Override
	/**
	 * Check if coordinates are the same
	 */
	public boolean equals(Object obj) {
		Coordinate other = (Coordinate) obj;
		if (col != other.col)
			return false;
		if (row != other.row)
			return false;
		return true;
	}
	
	/**
	 * Given a string [integer],[integer]
	 * create a new instance with the information
	 * @param s string with the information
	 * @return the coordinate instance
	 */
	public Coordinate valueOf(String s) {
		String[] aux=s.split(",");
		return new Coordinate(Integer.valueOf(aux[0]), Integer.valueOf(aux[1]));
	}
}
