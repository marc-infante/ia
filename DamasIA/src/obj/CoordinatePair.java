package obj;




public class CoordinatePair {
	
	protected Coordinate firstPair;
	protected Coordinate secondPair;
	/**
	 * @param row1: row of first pair
	 * @param col1: column of first pair
	 * @param row2: row of second pair
	 * @param col2: column of second pair
	 */
	public CoordinatePair(int row1, int col1, int row2, int col2) {
		this.firstPair= new Coordinate(row1, col1);
		this.secondPair=new Coordinate(row2, col2);
	}
	
	
	
	
	/**
	 * @param firstPair
	 * @param secondPair
	 */
	public CoordinatePair(Coordinate firstPair, Coordinate secondPair) {
		super();
		this.firstPair = firstPair;
		this.secondPair = secondPair;
	}




	/**
	 * @return the first pair of coordinates
	 */
	public Coordinate getFirstPair() {
		return firstPair;
	}
	
	/**
	 * @return the second pair of coordinates
	 */
	public Coordinate getSecondPair() {
		return secondPair;
	}
	
	/**
	 * 
	 * @param pair: info to set in first pair
	 */
	public void setFirstPair(Coordinate pair) {
		firstPair=pair.clone();
	}
	
	/**
	 * 
	 * @param pair:  info to set in second pair
	 */
	public void setSecondPair(Coordinate pair) {
		secondPair= pair.clone();
	}
	
	@Override
	public String toString() {
		return firstPair.toString()+";"+secondPair.toString();
	}


	@Override
	public boolean equals(Object obj) {
		CoordinatePair other = (CoordinatePair) obj;
		if (firstPair == null) {
			if (other.firstPair != null)
				return false;
		} else if (!firstPair.equals(other.firstPair))
			return false;
		return true;
	}
	
	/**
	 * Given a string [integer],[integer];[integer],[integer]
	 * create a new instance with the information
	 * @param s string with the information
	 * @return the coordinate instance
	 */
	public CoordinatePair valueOf(String s) {
		String[] aux = s.split(";");
		return new CoordinatePair(new Coordinate().valueOf(aux[0]),new Coordinate().valueOf(aux[1]));
	}
	
	public CoordinatePair clone() {
		CoordinatePair aux = new CoordinatePair(this.firstPair.clone(),this.secondPair.clone());
		return aux;
	}

	
	
}
