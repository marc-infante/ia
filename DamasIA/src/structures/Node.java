package structures;

import java.util.ArrayList;

import obj.CoordinatePair;

public class Node {
	public final static int LVL_SEARCH_MINIMAX=6; 
	public final static int LVL_SEARCH_ALFABETA=8; 
	
	
	/**
	 * Return if the actual node is in a max
	 * level of the tree
	 * @return
	 */
	public boolean isMax(int level) {
		if(level%2==0)
			return true;
		return false;
	}
	
	/**
	 * Check if the value toCompare is better than the
	 * reference
	 * @param reference: value saved
	 * @param toCompare: new value to compare
	 * @param level: level of the tree for know if is a max or a min
	 * @return true if is better or false if not
	 */
	public boolean isBetter(double reference, double toCompare,int level) {
		if(isMax(level)) {
			if(reference < toCompare) {
				return true;
			}
			return false;
		}else {
			if(reference > toCompare) {
				return true;
			}
			return false;
		}
	}
	
	/**
	 * Check if a colour has win the game 
	 * @param b: board at the moment to to the check
	 * @param colour: colour of the actual player
	 * @param secondWithoutMoves: indicates if it's the second consecutive
	 * time that a colour has no movements
	 * @param level: level of the tree for know if is a max or a min
	 * @return if is really the end of the game return infinite or -infinite, if not
	 * return a -200 
	 */
	public double controlVictory(Board b,String colour,boolean secondWithoutMoves,int level) {
		String aux;
		if(b.winColor()==null) {
			if(secondWithoutMoves) {
				aux=b.searchForWinner();
				if(aux==null) {
					return 100;
				}else if (aux.equals(colour)) {
					if(isMax(level)) return 100;
					else return -100;
				}else {
					if(isMax(level))return -100;
					else return 100;
				}
			}else {
				return -200;
			}
		}else if(b.winColor().equals(colour)) {
			if(isMax(level)) return 100;
			else return -100;
		}else {
			if(isMax(level))return -100;
			else return 100;
		}
	}
	
	/**
	 * Do the minimax algorithm 
	 * @param level: actual level of the tree
	 * @param b: board with the actual situation
	 * @param colour: colour of the actual player
	 * @param secondWithoutMoves: indicates if the last node has moves
	 * @return an heuristic
	 */
	public double minimaxSearch(int level,Board b,String colour,boolean secondWithoutMoves,int heuOp) {
		ArrayList<CoordinatePair> moves = b.movesPossibles(colour);
		double aux,ref,finish;
		if(isMax(level))
			ref = -100;
		else
			ref = 100;
		if(level==LVL_SEARCH_MINIMAX) {
			if(isMax(level))
				return b.selectHeuristic(heuOp, colour);
			else
				return b.selectHeuristic(heuOp, b.opponentColor(colour));
		}else if(moves == null){
			finish=controlVictory(b, colour,secondWithoutMoves,level);
			if(finish==-200)
				ref = minimaxSearch(level+1, b.clone(),b.opponentColor(colour),true,heuOp);
			else
				return finish;
		}else {
			for (CoordinatePair move : moves) {
				aux = minimaxSearch(level+1, b.cloneWithMove(move,colour),b.opponentColor(colour),false,heuOp);
				if(isBetter(ref,aux,level))
					ref=aux;
			}
		}
		
		return ref;
	}
	
	
	/**
	 * Do the alfa-beta algorithm
	 * @param level: actuaal level of the tree
	 * @param b: board with the actual situation
	 * @param colour: colour of the actual player
	 * @param secondWithoutMoves: inidicates if the last node has moves
	 * @param alfa: alfa values
	 * @param beta: beta values
	 * @return an array of two positions, the first is alfa-beta an the second the heuristic
	 */
	public double[] alfaBetaSearch(int level,Board b,String colour,boolean secondWithoutMoves,double alfa, double beta,int heuOp) {
		ArrayList<CoordinatePair> moves = b.movesPossibles(colour);
		double[] values= new double[2],aux = new double[2];
		double finish;
		int i=0;
		if(isMax(level))
			values[1] = -100;
		else
			values[1] = 100;
		if(level==LVL_SEARCH_ALFABETA) {
			if(isMax(level)) {
				values[0]=b.selectHeuristic(heuOp, colour);							
				values[1]=b.selectHeuristic(heuOp, colour);							
				return values;
			}else {
				values[0]=b.selectHeuristic(heuOp, b.opponentColor(colour));		
				values[1]=b.selectHeuristic(heuOp, b.opponentColor(colour));		
				return values;
			}
		}else if(moves == null){
			finish=controlVictory(b, colour,secondWithoutMoves,level);
			if(finish==-200) {
				values = alfaBetaSearch(level+1, b.clone(),b.opponentColor(colour),true,alfa,beta,heuOp);
				return values;
			}else {
				if(isMax(level))
					values[0]=alfa;
				else
					values[0]=beta;
				values[1]=finish;
				return values;
			}
		}else {
			while(i<moves.size() && alfa<beta) {
				aux = alfaBetaSearch(level+1, b.cloneWithMove(moves.get(i),colour),b.opponentColor(colour),false,alfa,beta,heuOp);
				if(isMax(level)) {
					if(aux[0] >= alfa)
						alfa=aux[0];
				}else {
					if(aux[0] <= beta)
						beta=aux[0];
				}
				if(isBetter(values[1], aux[1],level))
					values[1]=aux[1];
				i++;
			}
			
		}
		if(isMax(level)) {
			values[0]=alfa;
			return values;
		}else {
			values[0]=beta;
			return values;
		}
	}
	
}
