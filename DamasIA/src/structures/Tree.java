package structures;

import java.util.ArrayList;

import obj.CoordinatePair;

public class Tree {
	
	private Node initial;
	

	/**
	 * Constructor for the class Tree
	 */
	public Tree() {
		this.initial= new Node();
	}
	
	/**
	 * Start the minimax algorithm 
	 * @param colour: colour of the player
	 * @param situation: board with the actual situation
	 * @return the movement to do
	 */
	public CoordinatePair minimax(String colour,Board situation,int heuOp) {
		ArrayList<CoordinatePair> moves = situation.movesPossibles(colour);
		ArrayList<CoordinatePair> bestMoves =  new ArrayList<CoordinatePair>();
		double aux,ref=-100;
		if(moves != null) {
			for (CoordinatePair move : moves) {
				aux = initial.minimaxSearch(1, situation.cloneWithMove(move,colour),situation.opponentColor(colour),false,heuOp);
				if(initial.isBetter(ref,aux,0)) {
					ref=aux;
					bestMoves.clear();
					bestMoves.add(move.clone());
				}else if(aux == ref)
					bestMoves.add(move.clone());
			}
		}
		int index = (int)(Math.random()*bestMoves.size());
		return bestMoves.get(index);
	}
	
	/**
	 * Start the alfa-beta algorithm 
	 * @param colour: colour of the player
	 * @param situation: board with the actual situation
	 * @return the movement to do
	 */
	public CoordinatePair podaAlfaBeta(String colour, Board situation,int heuOp) {
		ArrayList<CoordinatePair> moves = situation.movesPossibles(colour);
		double[] aux= new double[2];
		int i=0;
		double alfa=-100,beta=100,ref=-100;
		CoordinatePair best = null;
		if(moves != null) {
			while(i< moves.size() && alfa<beta) {
				aux = initial.alfaBetaSearch(1, situation.cloneWithMove(moves.get(i),colour),situation.opponentColor(colour),false,alfa,beta,heuOp);
				if(aux[0]>=alfa) {
					alfa=aux[0];
				}
				if(initial.isBetter(ref, aux[1],0)) {
					ref=aux[1];
					best=moves.get(i);
				}
				i++;
			}
		}
		return best;
	}
	
	
	
	
}
