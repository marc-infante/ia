package structures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import obj.Coordinate;
import obj.CoordinatePair;
import obj.Dama;

public class Board {

	public final int SIZE = 8;
	public final int NUM_DAMAS = 12;
	public final String WHITE = "w";
	public final String BLACK = "b";
	public final String MOVE_LEFT = "L";
	public final String MOVE_RIGHT ="R";
	
	private int blackKills,whiteKills;
	private HashMap<String,ArrayList<Dama>> damas;
	
	
	public Board() {
		this.damas = new HashMap<String, ArrayList<Dama>>();
		this.blackKills=0;
		this.whiteKills=0;
		buildBoard();
	}
	
	
	
	/**
	 * @return the blackKills
	 */
	public int getBlackKills() {
		return blackKills;
	}



	/**
	 * @param blackKills the blackKills to set
	 */
	public void setBlackKills(int blackKills) {
		this.blackKills = blackKills;
	}



	/**
	 * @return the whiteKills
	 */
	public int getWhiteKills() {
		return whiteKills;
	}



	/**
	 * @param whiteKills the whiteKills to set
	 */
	public void setWhiteKills(int whiteKills) {
		this.whiteKills = whiteKills;
	}

	

	/**
	 * Return an instance with a copy of this
	 */
	public Board clone() {
		Board b= new Board();
		int i=0;
		String[] colors = {BLACK, WHITE};
		b.setBlackKills(this.blackKills);
		b.setWhiteKills(this.whiteKills);
		HashMap<String,ArrayList<Dama>> tab = new HashMap<String, ArrayList<Dama>>();
		ArrayList<Dama> damas;
		for (Map.Entry<String, ArrayList<Dama>> color : this.damas.entrySet()) {
			damas = new ArrayList<Dama>();
			for(Dama dama: color.getValue()) {
				damas.add(dama.clone());
			}
			tab.put(colors[i++], damas);
		}
		b.setDamas(tab);
		return b;
	}
	
	
	
	/**
	 * @return the damas
	 */
	public HashMap<String, ArrayList<Dama>> getDamas() {
		return damas;
	}


	/**
	 * @param damas the damas to set
	 */
	public void setDamas(HashMap<String, ArrayList<Dama>> damas) {
		this.damas = damas;
	}


	/**
	 * Initialise the coordinates of the pieces 
	 * to the initial position
	 */
	private void buildBoard() {
		ArrayList<Dama> w = new ArrayList<Dama>();
		ArrayList<Dama> b = new ArrayList<Dama>();
		for (int i = 0; i < SIZE; i++) {
			for (int j = 0; j < SIZE; j++) {
				if((i+j)%2!=0) {
					if(i<3) 
						b.add(new Dama(i, j));
					if(i>4)
						w.add(new Dama(i, j));
				}
			}
		}
		damas.put(BLACK, b);
		damas.put(WHITE, w);
	}
	
	/**
	 * Check if the parameter is in the board
	 * @param c coordinate to check
	 * @return true if is in or false if not
	 */
	public boolean isCoordinateValid(Coordinate c) {
		if(c.getRow()<0 || c.getRow()>=SIZE)
			return false;
		if(c.getCol()<0 || c.getCol()>=SIZE)
			return false;
		return true;
	}
	
	/**
	 * Do the move if can do it
	 * @param origin: origin coordinate
	 * @param kill: detects if is the second move (must be false at the call
	 * of the function)
	 * @param color: colour of the actual player
	 * @param move: move to do, right or left
	 * @return the new coordinate or null if the move is not valid
	 */
	public Coordinate move(Coordinate origin,boolean kill,String color,String move) {
		Coordinate aux = origin.clone();
		//CONTROL UP DOWN
		if(color.equals(BLACK))
			aux.setRow(aux.getRow()+1);
		else
			aux.setRow(aux.getRow()-1);
		//CONTROL LEFT RIGHT
		if(move.equals(MOVE_RIGHT))
			aux.setCol(aux.getCol()+1);
		else
			aux.setCol(aux.getCol()-1);
		//CHECK MOVEMENT
		if(isCoordinateValid(aux) ) {
			if(!isMyDama(aux, color)) {
				if(!isOpponentDama(aux, color))
					return aux;
				else if(!kill)
					return move(aux,!kill,color,move);
			}
		}
		return null;
	}
	
	
	
	/**
	 * Check if in the coordinate exist an opponent 
	 * dama
	 * @param c: coordinate to check
	 * @param colour: colour of the actual player
	 * @return true if exist a dama and false if not
	 */
	public boolean isOpponentDama(Coordinate c,String colour) {
		for (Dama dama : damas.get(opponentColor(colour))) {
			if(dama.getPosition().equals(c))
				return true;
		}
		return false;
	}
	
	/**
	 * Check if in the coordinate exist an own dama
	 * @param c: coordinate to check
	 * @param colour: colour of the actual player
	 * @return true if exist a dama and false if not
	 */
	public boolean isMyDama(Coordinate c, String colour) {
		for (Dama dama : damas.get(colour)) {
			if(dama.getPosition().equals(c))
				return true;
		}
		return false;
	}
	
	/**
	 * Check if the dama exist and if exists
	 * return an instance
	 * @param c: coordinate in which dama is
	 * @param color: color of the actual player
	 * @return the dama if exists or null if not
	 */
	public Dama getDama(Coordinate c,String color) {
		if(color.equals(WHITE)) {
			for (Dama dama : damas.get(WHITE)) {
				if(dama.getPosition().equals(c))
					return dama;
			}
		}else{
			for (Dama dama : damas.get(BLACK)) {
				if(dama.getPosition().equals(c))
					return dama;
			}
		}
		return null;
	}
	
	
	
	/**
	 * Check all the moves that can do a color and return an array
	 * with each one
	 * @param black: true for black moves o false for white moves
	 * @return an arraylist with the coordinates of the pieces that can move
	 */
	public ArrayList<CoordinatePair> movesPossibles(String color){
		ArrayList<CoordinatePair> moves = new ArrayList<CoordinatePair>();
		String[] movement =new String[]{MOVE_LEFT,MOVE_RIGHT};
		Coordinate aux = new Coordinate(0,0);
		for (Dama dama : this.damas.get(color)) {
			for(String move: movement) {
				aux = move(dama.getPosition(),false,color,move);
				if(aux != null) {
					moves.add(new CoordinatePair(dama.getPosition().getRow(), 
												dama.getPosition().getCol(),
												aux.getRow(),
												aux.getCol()));
				}
			}
		}
		if(moves.size()>0)
			return moves;
		else
			return null;
	}
	
	
	/**
	 * Given the origin coordinate and the next coordinate to move
	 * remove the first dama instance from the arraylist and create a new
	 * whit the next info.
	 * @param origen: origin coordinate
	 * @param next: coordinate to move
	 * @param colour: colour of the actual player
	 */
	public void moveDama(Coordinate origen, Coordinate next,String colour) {
		Coordinate kill = detectKill(origen, next);
		if(kill!=null) {
			removeDama(kill, opponentColor(colour));
			incrementKills(colour);
		}
		removeDama(origen,colour);
		this.damas.get(colour).add(new Dama(next.getRow(), next.getCol()));
		
	}
	
	
	/**
	 * Given one coordinate deletes it from the
	 * arraylist 
	 * @param toRemove: coordinate to remove
	 * @param colour: colour of the actual player
	 */
	public void removeDama(Coordinate toRemove,String colour) {
		for (int i = 0; i < this.damas.get(colour).size(); i++) {
			if(this.damas.get(colour).get(i).getPosition().equals(toRemove)) {
				this.damas.get(colour).remove(i);
			}
		}
		
	}
	
	/**
	 * Given a movement detects if has been a kill
	 * and return the coordinate of the dama killed
	 * @param origin: origin coordinate of the move
	 * @param next: final coordinate
	 * @return null if there is no kill or the coordinate of the piece
	 * killed
	 */
	public Coordinate detectKill(Coordinate origin,Coordinate next) {
		Coordinate aux = null;
		int difRows=next.getRow()-origin.getRow();
		int difCols=next.getCol()-origin.getCol();
		if(Math.abs(difCols)==2) {
			aux= new Coordinate(origin.getRow()+(difRows/2), origin.getCol()+(difCols/2));
		}
		return aux;
	}

	/**
	 * Given a movement and the colour that does
	 * this movement, return an instance of the board
	 * with a copy of this and the movement
	 * @param c: coordinates that explain the movement
	 * @param colour: colour of the actual player
	 * @return an instance of the board with the movement
	 */
	public Board cloneWithMove(CoordinatePair c,String colour) {
		Board aux = this.clone();
		aux.moveDama(c.getFirstPair(), c.getSecondPair(), colour);
		return aux;
	}
	
	/**
	 * Given one colour return the opponent colour
	 * @param color: colour of the actual player
	 * @return the opponent colour
	 */
	public String opponentColor(String color) {
		if(color.equals(WHITE))
			return BLACK;
		return WHITE;
	}
	
	/**
	 * 
	 * @return
	 */
	public String winColor() {
		if(blackKills == NUM_DAMAS)
			return BLACK;
		if(whiteKills == NUM_DAMAS)
			return WHITE;
		return null;
	}
	
	/**
	 * Given the colour of the team that 
	 * has a new kill increment his counter
	 * @param colour: actual team
	 */
	public void incrementKills(String colour) {
		if(colour.equals(BLACK))
			blackKills++;
		else
			whiteKills++;
	}

	/**
	 * Check who is the winner
	 * @param clone: board at the end of the game
	 */
	public String finishGame(Board clone) {
		String[] messageWinner= {"NEGRAS GANAN","BLANCAS GANAN","EMPATE"};
		String winner;
		winner=clone.searchForWinner();
		if(winner==null) 
			return messageWinner[2];
		else if(winner.equals(BLACK))
			return messageWinner[0];
		else
			return messageWinner[1];
		
		
	}


	/**
	 * If the colours has the same kills, check
	 * who is the winner
	 * @return the colour of the winner or null if there is a draw
	 */
	public String searchForWinner() {
		String winner= null;
		boolean founded =false;
		int black=0, white=0, indexB=7,indexW=0,total=0;
		if(blackKills>whiteKills)
			winner = BLACK;
		else if(whiteKills>blackKills)
			winner = WHITE;
		else {
			do {
				for (Dama blackPiece : damas.get(BLACK)) {
					if(blackPiece.getPosition().getRow()==indexB) {
						black++;
						total++;
					}
				}
				for (Dama whitePiece : damas.get(WHITE)) {
					if(whitePiece.getPosition().getRow()==indexW) {
						white++;
						total++;
					}
				}
				if(black>white) {
					winner=BLACK;founded=true;
				}else if(white>black) {
					winner=WHITE;founded=true;
				}else {
					black=0;white=0;indexB--;indexW++;
				}
			}while(!founded && total < (damas.get(BLACK).size()+damas.get(WHITE).size()));
		}
		return winner;
	}
	
	
	//HEURISTICA
	
	/**
	 * Given an option select the heuristic function to do
	 * @param option: 1, 2 or 3 the heuristics options
	 * @param colour: colour of the player that is doing the search algorithm
	 * @return an heuristic
	 */
	public double selectHeuristic(int option,String colour) {
		double heu=0;
		switch (option) {
		case 0:
			heu+=centerDamas(damas.get(colour));
			heu+=searchSpace(colour);
			break;
		case 1:
			heu+=detectActualWinner(colour);
			heu+= numberOfCoveredBacks(colour);
			break;
		case 2:
			heu+=damasDiff(colour);
			heu+=sacrificeDamas(colour);
			break;
		}
		return heu;
	}
	
	/**
	 * Check the position of all the damas and scores them 
	 * according how close are they to the center of the board
	 * @param damas: array with the damas
	 * @return an heuristic
	 */
	public double centerDamas(ArrayList<Dama> damas) {
		int limInf=0, limSup=SIZE-1;
		double heu=0;
		int row,col;
		for (Dama dama : damas) {
			row=dama.getPosition().getRow();
			col=dama.getPosition().getCol();
			if((row==limInf || row==limSup)||(col==limInf || col==limSup)) {
				heu+=1;
			}else if((row==limInf+1 || row==limSup-1)||(col==limInf+1 || col==limSup-1)) {
				heu+=2;
			}else if((row==limInf+2 || row==limSup-2)||(col==limInf+2 || col==limSup-2)) {
				heu+=3;
			}else
				heu+=4;
		}
		return heu;
	}
	
	/**
	 * Scores the position of the dama according
	 * how close is from the column with less opponent damas
	 * @param colour: colour of the player that is doing the search algorithm
	 * @return an heuristic
	 */
	public double searchSpace(String colour) {
		int col,res;
		double heu=0;
		for (Dama dama : damas.get(colour)) {
			col= colWithMinusOpponents(dama.getPosition(), opponentColor(colour));
			res = Math.abs(col-dama.getPosition().getCol());
			if(res!=0)
				heu+=1/res;
			else
				heu+=1.2;
		}
		return heu;
	}
	
	/**
	 * Given a dama coordinate and the colour return the column with
	 * less opponent damas 
	 * @param c: coordinate of the dama
	 * @param colour: colour of the dama
	 * @return the column with less opponent damas
	 */
	public int colWithMinusOpponents(Coordinate c,String colour) {
		int[] cols = {0,0,0,0,0,0,0,0};
		int min=12,col=0;
		if(colour.equals(BLACK)) {
			for (Dama dama : damas.get(colour)) {
				if(dama.getPosition().getRow()<c.getRow())
					cols[dama.getPosition().getCol()]++;
			}
		}else {
			for (Dama dama : damas.get(colour)) {
				if(dama.getPosition().getRow()>c.getRow())
					cols[dama.getPosition().getCol()]++;
			}
		}
		ArrayList<Integer> columns = new ArrayList<Integer>();
		for (int i : cols) {
			if(i<min) {
				min=i;
			}
		}
		for (int i = 0; i < cols.length; i++) {
			if(cols[i]==min) {
				columns.add(i);
			}
		}
		min=7;
		for (Integer column : columns) {
			int dist = Math.abs(column-c.getCol());
			if(dist<min) {
				min=dist;
				col=column;
			}
		}
		return col;
	}
	
	/**
	 * Detect who would be the winner if the game ended
	 * without the rule of who has more chips wins
	 * @param colour: colour of the player that is doing the search algorithm
	 * @return an heuristic
	 */
	public double detectActualWinner(String colour) {
		String winner=null;
		boolean founded =false;
		int black=0, white=0, indexB=7,indexW=0,total=0,diff=0;
		do {
			for (Dama blackPiece : damas.get(BLACK)) {
				if(blackPiece.getPosition().getRow()==indexB) {
					black++;
					total++;
				}
			}
			for (Dama whitePiece : damas.get(WHITE)) {
				if(whitePiece.getPosition().getRow()==indexW) {
					white++;
					total++;
				}
			}
			if(black>white) {
				winner=BLACK;founded=true;
			}else if(white>black) {
				winner=WHITE;founded=true;
			}else {
				black=0;white=0;indexB--;indexW++;
			}
		}while(!founded && total < (damas.get(BLACK).size()+damas.get(WHITE).size()));
		if(winner != null) {
			diff= Math.abs(black-white);
			switch(winner) {
			case(BLACK): 
				if(colour.equals(BLACK))
					return diff;
				else return -diff;
			case(WHITE):
				if(colour.equals(WHITE))
					return diff;
				else return -diff;
			}
		}
		return 0;
	}

	/**
	 * For each dama check if his back is covered and scores
	 * 1 point for each one 
	 * @param colour: colour of the player that is doing the search algorithm
	 * @return an heurisitic
	 */
	public double numberOfCoveredBacks(String colour) {
		Coordinate aux= new Coordinate();
		double heu=0;
		for (Dama dama : damas.get(colour)) {
			if(colour.equals(BLACK))
				aux.setRow(dama.getPosition().getRow()-1);
			else
				aux.setRow(dama.getPosition().getRow()+1);
			aux.setCol(dama.getPosition().getCol()+1);
			if(coveredBack(aux, colour))
				heu++;
			aux.setCol(dama.getPosition().getCol()-1);
			if(coveredBack(aux, colour))
				heu++;
		}
		return heu;
	}
	
	/**
	 * Check if in the coordinate there is a colour dama
	 * or it's off the board
	 * @param back: coordinate to check
	 * @param colour: colour that the dama should be
	 * @return true if is covered or false if not
	 */
	public boolean coveredBack(Coordinate back,String colour) {
		if(!isCoordinateValid(back) || isMyDama(back, colour))
			return true;
		return false;
	}

	/**
	 * Calculates the difference of damas
	 * @param colour: colour of the player that is doing the search algorithm
	 * @return an heuristic
	 */
	public double damasDiff(String colour) {
		if(colour.equals(BLACK)) 
			return (damas.get(BLACK).size()-damas.get(WHITE).size())*3;
		return (damas.get(WHITE).size()-damas.get(BLACK).size())*3;	
	}
	
	/**
	 * If a dama is adjoining with an opponent check if
	 * it can be killed and if can counter 
	 * @param colour: colour of the player that is doing the search algorithm
	 * @return an heuristic
	 */
	public double sacrificeDamas(String colour) {
		double heu=0;
		int next;
		Coordinate aux= new Coordinate();
		int sides[]= {1,-1}; 
		if(colour.equals(BLACK))
			next=1;
		else
			next=-1;
		for (Dama dama : damas.get(colour)) {
			//AUX = FICHA DE DELANTE
			aux.setRow(dama.getPosition().getRow()+next);
			for (int side : sides) {
				aux.setCol(dama.getPosition().getCol()+side);
				//SI EN LA CASILLA HAY UNA FICHA CONTRARIA
				if(!isCoordinateValid(aux) || isOpponentDama(aux, colour)) {
					//AUX = FICHA OPUESTA A LA CONTRARIA
					aux.setRow(dama.getPosition().getRow()-(2*next));
					aux.setCol(dama.getPosition().getCol()-(2*side));
					//SI EN LA CASILLA HAY UNA FICHA NUESTRA O FUERA DEL TABLERO
					if(coveredBack(aux, colour)) {
						heu+=0.5;
					//SI HAY UN CAMPO VACIO
					}else if (!isOpponentDama(aux, colour)) {
						//AUX = CASILLA ANTERIOR SIGUIENDO LA DIAGONAL
						aux.setRow(dama.getPosition().getRow()-next);
						aux.setCol(dama.getPosition().getCol()-side);
						if(isMyDama(aux, colour)) {
							heu+=1.5;
						}else {
							heu-=1;
						}
						//AUX = CASILLA ANTERIOR EN LA OTRA DIAGONAL
						aux.setCol(dama.getPosition().getCol()+(2*side));
						if(isMyDama(aux, colour)) {
							//AUX = CASILLA POSTERIOR EN LA OTRA DIAGONAL
							aux.setRow(dama.getPosition().getRow()+(2*next));
							aux.setCol(dama.getPosition().getCol()-(2*side));
							if(!isMyDama(aux, colour) && !isOpponentDama(aux, colour) && isCoordinateValid(aux))
								heu+=1.5;
						}
					}
				}
			}
			
		}
		return heu;
	}
	

}
